import { ReforestAdminPage } from './app.po';

describe('reforest-admin App', function() {
  let page: ReforestAdminPage;

  beforeEach(() => {
    page = new ReforestAdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
