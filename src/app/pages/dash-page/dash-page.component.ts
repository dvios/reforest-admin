import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dash-page',
  templateUrl: './dash-page.component.html',
  styleUrls: ['./dash-page.component.css']
})
export class DashPageComponent implements OnInit {

  posts: [{
    id: string;
    images: Array<string>;
    title: string;
    description: string;
    location: { name: string, coordinates: string };
    postedById: string;
  }];

  constructor() {
  }

  ngOnInit() {
  }

}
