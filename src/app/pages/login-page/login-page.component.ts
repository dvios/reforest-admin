import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css'],
  providers: [AuthService]
})
export class LoginPageComponent implements OnInit {
  private authData: { email: string, password: string, rememberMe: boolean };

  constructor(private authServ: AuthService) {
    this.authData = {email: '', password: '', rememberMe: false};
  }

  ngOnInit() {
  }

  loginNormal() {
    this.authServ.loginNormal(this.authData).subscribe((data: any) => {
      alert(JSON.stringify(data));
    });
  }

}
