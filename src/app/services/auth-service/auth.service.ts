import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import * as config from '../../configs';
import { Observable } from 'rxjs';

@Injectable()
export class AuthService {

  constructor(private http: Http) {
  }

  loginNormal(authData) {
    localStorage.removeItem('authToken');
    return new Observable(observer => {
      this.http.post(config.apiUrl + '/rest/user/login?method=normal', authData)
        .map(data => data.json())
        .subscribe((data: any) => {
          if (data.success) {
            if (authData.rememberMe) {
              // save the token to localStorage
              localStorage.setItem('authToken', data.data.token);
            }
            observer.next(data);
          } else {
            observer.next({success: false});
          }
        });
    });
  }

  validateToken() {

  }

  logout() {
    localStorage.clear();
  }

}
