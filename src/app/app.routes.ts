import { Routes, RouterModule } from '@angular/router';
import {LoginPageComponent} from "./pages/login-page/login-page.component";
import { DashPageComponent } from './pages/dash-page/dash-page.component';


/*
 * Application routes should go here
 * All the child routes should be wrapped by a parent component for simplicity and separation
 */
const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: 'home',
    component: DashPageComponent,
  },
  // {
  //   path: '**',
  //   // component: Four04Component
  // },
];

export const routing = RouterModule.forRoot(appRoutes);
